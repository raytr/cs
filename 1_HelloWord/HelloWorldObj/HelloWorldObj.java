public class HelloWorldObj{
	public static void main (String [] args){
        //Single messages through MsgWriter
		MsgWriter hWriter = new MsgWriter("Hi");
		MsgWriter bWriter = new MsgWriter("Bye");
        //Repeated ones through MsgRepeater
		MsgRepeater helloWriter = new MsgRepeater(5 , "Hello World");

		hWriter.print();
		helloWriter.printMsg();
		bWriter.print();
	}
}
