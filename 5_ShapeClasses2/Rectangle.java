/**
 * Rectangle represents a rectangle with an xy position defined by its top left corner
 * Fields: x, y, width, height
 * Methods: area, perimeter, toString
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/10/18
 */
public class Rectangle extends Shape{
    public Rectangle(double x, double y, double width, double height){
        super(x,y,width,height);
    }
    public double perimeter(){
        return 2*width + 2*height;
    }
}
