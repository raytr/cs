/**
 * ShapeTester tests Shape subclasses
 * @author: Raymond Tran
 * @date: 9/10/18
 * @version: 1.0
 * @date: 9/10/18
 */
public class ShapeTester{
    public static void main(String[] args){
        //Things that don't work
        //RightTriangle[] rt = new RightTriangle[3];
        //rt[1] = new Rectangle(5,5,5,5);
        //Rectangle rect = new Shape(5,5,5,5);
        //Circle[] circ = new Circle[3];
        //circ[1] = new Rectangle(5,5,5,5);
        //
        //Rectangle[] rect = new Rectangle[3];
        //rect[1] = new Circle(5,5,5);

        Shape[] shapes = new Shape[]{
            new Rectangle(5,5,5,5),
            new RightTriangle(2.3,5.3,5.12,5),
            new Circle(5.3,5,5)
        };

        for (int i=0;i<shapes.length;i++){
            System.out.println(shapes[i]);
            System.out.println("----");
        }
    }
}
