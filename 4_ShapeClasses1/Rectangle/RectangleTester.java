/**
 * RectangleTester tests a Rectangle
 * @author:Raymond Tran
 * @date:9/04/18
 */
public class RectangleTester{
    public static void main(String[] args){
        Rectangle myRectangle = new Rectangle(4.0,5.0,20,100);
        System.out.println("Rectangle Area: " + myRectangle.getArea());
        System.out.println("Rectangle Perimeter: " + myRectangle.getPerimeter());
        System.out.println("Rectangle toString: " + myRectangle.toString());
    
    
    
    }
}
