/**
 * Rectangle represents a rectangle with an xy position defined by its top left corner
 * Fields: x, y, width, height
 * Methods: getArea, getPerimeter, toString
 * @author: Raymond Tran
 * @date: 8/31/18
 */
public class Rectangle{
    private double x;
    private double y;
    private double width;
    private double height;

    public Rectangle(double x, double y, double width, double height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    public double getArea(){
        return width*height;
    }
    public double getPerimeter(){
        return (2 * width) + (2 * height);
    }
    public String toString(){
        return "Rectangle: x: " + x + " y: " + y + " width: " + width + "height: " +height;
    }

    public double getX(){return x;}
    public double getY(){return y;}
    public double getWidth(){return width;}
    public double getHeight(){return height;}

    public void setX(double X){x = X;}
    public void setY(double Y){y = Y;}
    public void setWidth(double w){
        if (w>0) width = w;
        else throw new Error("Width cannot be negative");
    }
    public void setHeight(double h){
        if (h>0) height = h;
        else throw new Error("Height cannot be negative");
    }


}
