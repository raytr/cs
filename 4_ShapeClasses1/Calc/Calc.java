/**
 * Calc is used to perform simple math on two doubles
 * Fields: num1, num2 OR double[2]
 * Methods: 
 *      setAB, setA, setB, getAB, getA, getB
 *      getAplusB,  getAminusB, getBminusA
 *      getAtimesB, getAdivB,   getBdivA
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/04/18
 */
public class Calc{
    private double[] doubleAB = new double[2];
    //Constructors
    public Calc(double[] nums){
        if (nums.length != 2) throw new Error("Array length must be two");
        for (int i=0;i<2;i++){
            doubleAB[i] = nums[i];
        }
    }
    public Calc(double num1,double num2){
        doubleAB[0] = num1;
        doubleAB[1] = num2;
    }
    //Public
    public void setA(double a){
        doubleAB[0] = a;
    }

    public void setB(double b){
        doubleAB[1] = b;
    }
 
    public void setAB(double[] ab){
        if (ab.length != 2) throw new Error("Array length must be two");
        for (int i=0;i<2;i++){
            doubleAB[i] = ab[i];
        }
    
    }
    public void setAB(double a,double b){
        doubleAB[0] = a;
        doubleAB[1] = b;
    }
    public double[] getAB(){
        return doubleAB;
    }
    public double getA(){
        return doubleAB[0];
    }
    public double getB(){
        return doubleAB[1];
    }
    public double getAplusB(){
        return doubleAB[0] + doubleAB[1];
    }
    public double getAtimesB(){
        return doubleAB[0] * doubleAB[1];
    }
    public double getAdivB(){
        return doubleAB[0] / doubleAB[1];
    }
    public double getBdivA(){
        return doubleAB[1] / doubleAB[0];
    }
    public double getAminusB(){
        return doubleAB[0] - doubleAB[1];
    }
    public double getBminusA(){
        return doubleAB[1] - doubleAB[0];
    }
}
