/**
 * Player creates a guessing game player
 * Fields: wins, losses, previousGuesses
 * Public Methods: takeTurn, resetOneGame, addToPreviousGuesses
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/25/18
 */
public class Player{
    private String name;
    private int[] previousGuesses;
    private int wins;
    private int losses;

    public Player(String name){
	previousGuesses = new int[0];
	this.name = name;
	wins = 0;
	losses = 0;
    }

    //Returns the player's guess
    public int takeTurn(GUI gui){
	int guess = gui.receiveIntReply();
	return guess;
    }
    //Resets the fields used in one game (not multiple games)
    public void resetOneGame(){
	previousGuesses = new int[0];
    }
    //Adds another int to the previous guesses array
    public void addToPreviousGuesses(int guess){
	int[] newArr = new int[previousGuesses.length + 1];
	for (int i=0;i<previousGuesses.length;i++){
	    newArr[i] = previousGuesses[i];
	}
	newArr[previousGuesses.length] = guess;
	previousGuesses = newArr;
    }
    public int[] getPreviousGuesses(){
	return previousGuesses;
    }
    public String toString(){
	return this.name;
    }
    public void setLosses(int l){
	losses = l;
    }
    public int getLosses(){
	return losses;
    }
    public void setWins(int w){
	wins = w;
    }
    public int getWins(){
	return wins;
    }
    public void setName(String name){
	this.name = name;
    }
    public String getName(){
	return name;
    }
}
