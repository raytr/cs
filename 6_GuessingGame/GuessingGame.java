/**
 * GuessingGame runs the game
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/25/18
 */
public class GuessingGame {
	
	public static void main (String[] args) {
		Game game1 = new Game();
		boolean playAgain = true;
		while (playAgain){
			game1.reset();
			playAgain = game1.play();
		}
	}
	
	
	
}
