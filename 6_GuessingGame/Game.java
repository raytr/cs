/**
 * Game creates a new guessing game instance
 * Public Fields: none public
 * Public Methods: reset, play
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/25/18
 */
public class Game{
    Player[] players;
    Scoreboard scoreboard;
    GUI gui;
    int target; //Target Guess
    int whoseTurn; //Index of the current player's turn
    int previousWinner; //Index of the last winner
    int[] bounds; //Bounds of the guess
    boolean noWinner; //Whether or not there is a winner
    public Game(){
	previousWinner = 0;
	bounds = new int[2];
	players = new Player[0];
	scoreboard = new Scoreboard(players);
	gui = new GUI();
	noWinner = true;
    }

    //Resets the game
    public void reset(){
	//If it's not the first time through
	if (players.length != 0){
	    gui.displayMsg("Do you want to reset players?");
	    if (gui.receiveBooleanReply()){
		previousWinner = 0;
		players = promptForPlayers();
		scoreboard.setPlayers(players);
	    }else{
		for (int i=0;i<players.length;i++){
		    players[i].resetOneGame();
		}
	    }
	}
	bounds = new int[2];
    }
    //Returns true if the player wants to play again, false otherwise
    public boolean play(){
	//Start of the game
	gui.displayMsg("Welcome to the Guessing Game!");
	//Populate player array
	if (players.length == 0){
	    players = promptForPlayers();
	    scoreboard.setPlayers(players);
	}
	//Ask for bounds
	bounds = promptForBounds();
	target = generateRandomInt(bounds[0],bounds[1]);

	//Play the game
	whoseTurn = previousWinner;
	noWinner = true;
	while (noWinner){
	    if (whoseTurn == players.length) whoseTurn = 0;
	    Player currentPlayer = players[whoseTurn];
	    gui.displayMsg("--------------");
	    
	    gui.displayMsg(currentPlayer.getName() + "'s turn!");
	    gui.displayMsg(currentPlayer.getName() + " previous guesses");
	    String guessesString = "";
	    for (int i = 0;i < currentPlayer.getPreviousGuesses().length;i++){
		guessesString += currentPlayer.getPreviousGuesses()[i] + " ";
	    }
	    gui.displayMsg(guessesString);
	    gui.displayMsg("Please enter a guess");

	    int guess = currentPlayer.takeTurn(gui);
	    while (bounds[0] > guess || bounds[1] < guess){
		gui.displayMsg("Out of bounds. Please enter a guess between " + bounds[0] + " and " + bounds[1]);
		guess = players[whoseTurn].takeTurn(gui);
	    }	
	    currentPlayer.addToPreviousGuesses(guess);
	    
	    if (guess == target){
		noWinner = false;
		for (int i=0;i<players.length;i++){
		    if (i != whoseTurn){
			players[i].setLosses(players[i].getLosses() + 1);
		    }
		}
		players[whoseTurn].setWins(players[whoseTurn].getWins() + 1);
		previousWinner = whoseTurn;
	    }
	    else if (guess > target) gui.displayMsg("Too high.");
	    else gui.displayMsg("Too low.");
	    whoseTurn++;
	}


	gui.displayMsg(scoreboard.fancyToString());
	//End of the game
	gui.displayMsg("Do you want to play again?");
	return gui.receiveBooleanReply(); 
    }


    
    //Returns a random int from lowerBounds to higherBound, inclusive
    private int generateRandomInt(int lowerBound,int higherBound){
	return lowerBound + (int)(Math.random() * ((higherBound - lowerBound + 1)));
    }
    
    //Returns an int array[2], such that array[0] <= array[1]
    private int[] promptForBounds(){
	int bounds[] = new int[2];
	gui.displayMsg("Please enter the lower guessing bound.");
	bounds[0] = gui.receiveIntReply();
	gui.displayMsg("Please enter the upper guessing bound.");
	do {
	    bounds[1] = gui.receiveIntReply();
	    if (bounds[1] < bounds[0]){
		gui.displayMsg("Invalid. Please enter a number greater than " +bounds[0]);
	    }
	} while (bounds[1] < bounds[0]);
	return bounds;
    }

    //Returns a player array
    private Player[] promptForPlayers(){
	gui.displayMsg("Let's add some players!");
	boolean stillAdding = true;
	int playerCount=0;
	Player[] tempArray = new Player[9999];
	while (stillAdding){
	    gui.displayMsg("Please input the name of the player");
	    String playerName = gui.receiveStringReply();
	    playerCount++;
	    //Append new player to the temp array
	    int emptyIndex = 0;
	    while (tempArray[emptyIndex] != null) emptyIndex++;
	    tempArray[emptyIndex] = new Player(playerName);

	    gui.displayMsg("Current Players: ");
	    for (Player player : tempArray){
		if (player != null) gui.displayMsg("\t" + player.toString());
	    }
	    gui.displayMsg("Do you want to add another player?");
	    stillAdding = gui.receiveBooleanReply(); 
	}

	//Turn the 9999 length array into a proper sized one
	players = new Player[playerCount];
	for (int i=0;i<players.length;i++){
	    players[i] = tempArray[i];
	}
	return players;
    }
}
