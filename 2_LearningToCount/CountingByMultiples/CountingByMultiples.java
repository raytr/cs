public class CountingByMultiples{
    public static void main(String[] args){
        System.out.println("Counting to 100 by 20s");
        showCounting(20,100);
        System.out.println("Counting to 30 by 4s");
        showCounting(4,30);
    }
    private static void showCounting(int countBy,int max){
        int count = 0;
        while (count <= max){
            System.out.println(count);
            count += countBy;
        }
    }
}
