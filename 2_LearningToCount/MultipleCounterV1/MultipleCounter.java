public class MultipleCounter{
    private int countBy;
    private int startVal;
    private int endVal;

    MultipleCounter(int countBy,int startVal,int endVal){
        this.countBy = countBy;
        this.startVal = startVal;
        this.endVal = endVal;
    }

    public void showCounting(){
        int count = startVal;
        while (count <= endVal){
            System.out.println(count);
            count += countBy;
        }
    }

    public int getCountBy(){
        return countBy;
    }

    public int getStartVal(){
        return startVal;
    }

    public int getEndVal(){
        return endVal;
    }
}

    

