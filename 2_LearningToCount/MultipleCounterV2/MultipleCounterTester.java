public class MultipleCounterTester{
    public static void main(String[] args){
        MultipleCounter mCounters[] = {
            new MultipleCounter(5,0,35),
            new MultipleCounter(-5,35,0),
            //Error testing
            //new MultipleCounter(-2,0,10),
            //new MultipleCounter(2,10,0)
            
        };
        for (MultipleCounter mc : mCounters){
            System.out.println("Counting from " + mc.getStartVal() +" to "+ mc.getEndVal()
                    +" by " + mc.getCountBy() + "'s");
            mc.showCounting();
        }
    }
}
