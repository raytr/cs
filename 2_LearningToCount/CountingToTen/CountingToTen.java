public class CountingToTen{
    public static void main(String[] args){
        int count = 1;
        System.out.println(count); //1
        count++;
        System.out.println(count); //2
        count++;
        System.out.println(count); //3 
        count++;
        System.out.println(count); //4
        count++;
        System.out.println(count); //5
        count++;
        System.out.println(count); //6 
        count++;
        System.out.println(count); //7
        count++;
        System.out.println(count); //8
        count++;
        System.out.println(count); //9
        count++;
        System.out.println(count); //10
    
    
    }
}
