/**
 * MultiTable creates a 12x12 multiplication table 
 * @author: Raymond Tran
 * @version: 2.0
 * @date: 8/29/18
 */
public class MultiTable2{
    public static void main(String[] args){
        printTable(12);

        //Testing error
        //printTable(13);

    }

    private static void printTable(int xyMax){
        if (xyMax > 12) throw new Error("XY max cannot be greater than 12");
        int xMax = xyMax;
        int yMax = xyMax;
        int maxDigitLength = String.valueOf(yMax*xMax).length();

        for (int i=1;i<=xMax;i++){
            for (int j=1;j<=yMax;j++){

                String finalPrint = String.valueOf(i * j);
                int length = String.valueOf(i*j).length();
                while (length < maxDigitLength){
                    length ++;
                    finalPrint += " ";
                }
                System.out.print(finalPrint + " ");

            }
            System.out.println("");
        }
    }
}
