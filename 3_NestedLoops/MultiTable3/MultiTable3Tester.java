/** 
 * MultiTable3Tester tests MultiTable3
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 8/29/18
 */
public class MultiTable3Tester{
    public static void main(String[] args){

        MultiTable3 mt1 = new MultiTable3(12);
        System.out.println(mt1.getTableString());

        //Error testing
        //MultiTable3 mt2 = new MultiTable3(13);
        //mt2.printTable();

    }
}
