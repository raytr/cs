/**
 * Calc is used to perform simple math on two integers
 * Fields: int1, int2
 * Methods: 
 *      setA, setB, getA, getB
 *      getAplusB,  getAminusB, getBminusA
 *      getAtimesB, getAdivB,   getBdivA
 * @author: Raymond Tran
 * @version: 2.0
 * @date: 8/29/18
 */
public class Calc{
    int intA;
    int intB;
    Calc(int int1,int int2){
        intA = int1;
        intB = int2;
    }

    //Public
    public void setA(int a){
        intA = a;
    }

    public void setB(int b){
        intB = b;
    }
    public int getA(){
        return intA;
    }
    public int getB(){
        return intB;
    }
    public int getAplusB(){
        return intA + intB; 
    }
    public int getAtimesB(){
        return intA * intB; 
    }
    public int getAdivB(){
        return intA / intB;
    }
    public int getBdivA(){
        return intB / intA;
    }
    public int getAminusB(){
        return intA - intB;
    }
    public int getBminusA(){
        return intB - intA;
    }
}

   
